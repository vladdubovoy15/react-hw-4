import React, {memo, useEffect} from 'react';
import Loader from '../../components/Loader/Loader';
import Card from "../../components/Card/Card";
import './Cards.scss';
import {useDispatch, useSelector} from "react-redux";
import operations from "../../redux/operations";
import actions from "../../redux/actions";

const Cards = () => {
  const dispatch = useDispatch();
  const {isLoading, cards} = useSelector(state => state.app);

  useEffect(() => {
    cards.length === 0
        ? dispatch(operations.fetchCards())
        : dispatch(actions.setLoader(false));
  }, [cards, dispatch])

  return (
      <>
        {isLoading
            ? <Loader/>
            : <ul className={'cards-container'}>
                {cards.map(card => <Card key={card.id} card={card}/>)}
              </ul>
        }
      </>
  );
};

export default memo(Cards);