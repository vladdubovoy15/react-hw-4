import React, {memo} from 'react';
import Card from "../../components/Card/Card";
import {useSelector} from "react-redux";

const Cart = () => {
  const {cart} = useSelector(state => state.cart);

  return (
      <>
        {cart.length ?
            <ul className={'cards-container'}>{cart.map(card =>
                <Card key={card.id}
                      card={card}
                      trashCan={true}
                />)}
            </ul>
            : <h3 className={"red-text center"}>There are no products in the cart</h3>
        }
      </>)
}

export default memo(Cart);