import React from 'react';
import './PageIsNotFound.scss';

const PageIsNotFound = (props) => (
    <div className='wrapper'>
      <h2>Ops! Something went wrong.</h2>
      <p>Try refreshing the page or contact our support team</p>
      <a href={"mailto:support@gmail.com"}>support@gmail.com</a>
    </div>
);

export default PageIsNotFound;