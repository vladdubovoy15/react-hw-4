import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => (
    <header>
      <nav>
        <div className="nav-wrapper">
          <Link to="/" className="brand-logo center">Mobile Store</Link>
          <ul id="nav-mobile" className="left hide-on-med-and-down">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/cart">Cart</Link></li>
            <li><Link to="/favorite">Favorite</Link></li>
          </ul>
        </div>
      </nav>
    </header>
);

export default Header;