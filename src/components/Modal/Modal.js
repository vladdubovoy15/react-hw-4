import React from 'react';
import classes from './Modal.module.scss';
import '../../style/style.scss';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import actions from '../../redux/actions';
import Button from "../Button/Button";

const Modal = ({header}) => {
  const dispatch = useDispatch();

  return (
      <div className={classes["modal-overlay"]} onClick={() => dispatch(actions.setCartFromLocalStorage())}>
        <div className={classes.wrapper} onClick={(e) => e.stopPropagation()}>
          <header className={classes.header}>
            <h2 className={classes.title}>{header}</h2>
            <button className={classes["close-button"]} onClick={() => dispatch(actions.setCartFromLocalStorage())}>X
            </button>
          </header>
          <div className={classes["action-wrapper"]}>
            <Button backgroundColor={"#b3382c"}
                    text={"Ok"}
                    confirm={true}
            />
            <Button backgroundColor={"#b3382c"}
                    text={"Cancel"}
            />
          </div>
        </div>
      </div>)
};

Modal.propTypes = {
  header: PropTypes.string,
  modalHandler: PropTypes.func,
  cancel: PropTypes.bool,
  actions: PropTypes.element,
}

export default Modal;