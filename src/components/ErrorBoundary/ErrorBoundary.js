import React, {Component} from 'react';
import PageIsNotFound from "../../pages/PageIsNotFound/PageIsNotFound";

class ErrorBoundary extends Component {
  state = {
    errorPresent: false
  }

  static getDerivedStateFromError(error) {
    console.log(error)
    return {errorPresent: true}
  }

  render() {
    const {children} = this.props;
    const {errorPresent} = this.state;

    return errorPresent ? <PageIsNotFound/> : children
  }
}

export default ErrorBoundary;