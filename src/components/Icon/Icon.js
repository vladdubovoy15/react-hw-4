import React from 'react';
import * as Icons from '../../theme';
import './Icon.scss';
import PropTypes from "prop-types";
import {useDispatch} from "react-redux";
import actions from "../../redux/actions";

const Icon = ({type, color, filled, className, card}) => {
  const jsx = Icons[type];
  const dispatch = useDispatch();

  return (
      <>
        {
          jsx &&
          <div className={`icon icon--${type} ${className}`}
               onClick={() => dispatch(actions.setFavorite(card))}>
            {jsx({color, filled})}
          </div>
        }
      </>
  )
}

Icon.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  filled: PropTypes.bool,
  className: PropTypes.string,
  card: PropTypes.object,
  favoriteHandler: PropTypes.func,
}

Icon.defaultProps = {
  type: "star",
  filled: false
}

export default Icon;