import React, {memo, useCallback} from 'react';
import './Card.scss';
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import actions from "../../redux/actions";

const Card = ({card, trashCan}) => {
  const {title, price, color, img, code, id} = card;
  const {favorite} = useSelector(state => state.favorite);
  const {cart} = useSelector(state => state.cart);
  const dispatch = useDispatch();

  const addToCartHandler = useCallback(() => {
    if (!cart.find(e => e.id === id)) {
      dispatch(actions.addItemToCart(card))
    }
  }, [cart, id, card, dispatch])

  return (
      <li className='card z-depth-2'>
        <h3 className='card__title'>{title}</h3>
        <img src={img}
             className={"card__image"}
             alt={title}
             width={200}
             height={240}/>
        <ul>
          <li>code: {code}</li>
          <li>color: {color}</li>
          <li>price: {price}</li>
        </ul>
        <button className={"card__button red waves-effect waves-light"}
                onClick={addToCartHandler}>Add to cart
        </button>
        <Icon filled={Boolean(favorite.find(item => item.id === id))}
              className={"favorite"}
              card={card}
        />
        {trashCan && <i className="material-icons trash-can"
                        onClick={() => dispatch(actions.removeItemFromCart(card))}>delete</i>
        }
      </li>
  )
};

Card.propTypes = {
  card: PropTypes.object,
  modalHandler: PropTypes.func,
  favoriteHandler: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.object),
  deleteHandler: PropTypes.func,
}

Card.defaultProp = {
  trashCan: false
}

export default memo(Card);