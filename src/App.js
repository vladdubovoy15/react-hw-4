import React from "react";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import './style/style.scss';
import AppRouter from "./components/AppRouter/AppRouter";
import Modal from "./components/Modal/Modal";
import {useSelector} from "react-redux";

const App = () => {
  const {isOpen, header} = useSelector(state => state.app.modal);

  return (
      <ErrorBoundary>
        <Header/>
        <main className={"main"}>
          <AppRouter/>
        </main>
        <Footer/>
        {isOpen && <Modal header={header}/>}
      </ErrorBoundary>
  );
}

export default App;
